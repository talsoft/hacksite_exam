<?php
return array (
    'name' => 'faq',
    'type' => 'controller',
    'technology' => 'generic',
    'mapped_to' => 'faq',
    'storage_role' => 'root',
    'vulnerabilities' => 
    array (
        'vuln_list' => 
        array (
            'PHPSessionIdOverflow' => 
            array (
                'enabled' => false,
                'on_corrupted_id' => 'fix',
            ),
        ),
    ),
    'children' => 
    array (
        'index' => 
        array (
            'name' => 'index',
            'type' => 'action',
            'technology' => 'generic',
            'mapped_to' => 'index',
            'fields' => 
            array (
                0 => 
                array (
                    'name' => 'userQuestion',
                    'source' => 'body',
                    'vulnerabilities' => 
                    array (
                        'vuln_list' => 
                        array (
                            'SQL' => 
                            array (
                                'enabled' => false,
                                'blind' => false,
                            ),
                            'XSS' => 
                            array (
                                'enabled' => false,
                                'stored' => false,
                            ),
                        ),
                    ),
                ),
                1 => 
                array (
                    'name' => 'userEmail',
                    'source' => 'body',
                ),
            ),
        ),
        'view' => 
        array (
            'name' => 'view',
            'type' => 'action',
            'technology' => 'generic',
            'mapped_to' => 'view',
            'fields' => 
            array (
                0 => 
                array (
                    'name' => 'id',
                    'source' => 'param',
                    'vulnerabilities' => 
                    array (
                        'vuln_list' => 
                        array (
                            'XSS' => 
                            array (
                                'enabled' => false,
                                'stored' => false,
                            ),
                        ),
                    ),
                ),
            ),
            'vulnerabilities' => 
            array (
                'vuln_list' => 
                array (
                    'PHPSessionIdOverflow' => 
                    array (
                        'enabled' => false,
                        'on_corrupted_id' => 'exception',
                    ),
                ),
            ),
        ),
    ),
);