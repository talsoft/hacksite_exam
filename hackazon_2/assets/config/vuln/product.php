<?php
return array (
    'name' => 'product',
    'type' => 'controller',
    'technology' => 'web',
    'mapped_to' => 'product',
    'storage_role' => 'root',
    'vulnerabilities' => 
    array (
        'vuln_list' => 
        array (
            'CSRF' => 
            array (
                'enabled' => false,
            ),
        ),
    ),
    'children' => 
    array (
        'view' => 
        array (
            'name' => 'view',
            'type' => 'action',
            'technology' => 'web',
            'mapped_to' => 'view',
            'fields' => 
            array (
                0 => 
                array (
                    'name' => 'id',
                    'source' => 'any',
                    'vulnerabilities' => 
                    array (
                        'vuln_list' => 
                        array (
                            'IntegerOverflow' => 
                            array (
                                'enabled' => false,
                                'transform_strategy' => 'set_custom',
                                'custom_value' => -2,
                                'action_on_not_numeric' => 'filter',
                            ),
                            'SQL' => 
                            array (
                                'enabled' => false,
                                'blind' => false,
                            ),
                            'XSS' => 
                            array (
                                'enabled' => false,
                                'stored' => false,
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
);